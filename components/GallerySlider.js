import React, {useState, useEffect} from "react";
import Swiper from 'react-id-swiper';
import Lightbox from 'react-image-lightbox';
import {disablePageScroll, enablePageScroll} from 'scroll-lock';

export const GallerySlider = props => {
    const {aboutGallery} = props;

    const [slideIndexCurrent, setSlideIndexCurrent] = useState(0);
    const [photoIndex, setPhotoIndex] = useState(0);
    const [isOpenLightBox, setOpenLightBox] = useState(false);
    const [swiper, updateSwiper] = useState(null);

    const cls = {
        'gallery-slider': ['gallery-slider']
    };

    const initialSettingsSlider = {
        speed: 500,
        slidesPerView: 1,
        direction: 'horizontal',
        watchSlidesProgress: true,
        watchSlidesVisibility: true,
        watchOverflow: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        },
        navigation: {
            el: '.swiper-button',
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
            hideOnClick: true
        },
        breakpoints: {
            1024: {
                direction: 'vertical',
            },
            1366: {
                speed: 1500,
                slidesPerColumn : 2,
                direction: 'vertical',
                allowTouchMove: false,
            }
        }
    };

    const handleClickSliderLeaf = index => {
        setPhotoIndex(index);
        setOpenLightBox(true);
    };

    useEffect(() => {
        if (swiper !== null) {
            swiper.update();

            swiper.on('transitionStart', function () {
                setSlideIndexCurrent(swiper.realIndex);

                if (window.matchMedia("(min-width: 1366px)").matches) {
                    swiper.navigation.nextEl.classList.add('swiper-button-disabled');
                    swiper.navigation.prevEl.classList.add('swiper-button-disabled');
                }
            });

            swiper.on('slideChangeTransitionEnd', function () {
                if (window.matchMedia("(min-width: 1366px)").matches) {
                    if (!swiper.isEnd) {
                        swiper.navigation.nextEl.classList.toggle('swiper-button-disabled');
                    }

                    if (!swiper.isBeginning) {
                        swiper.navigation.prevEl.classList.toggle('swiper-button-disabled');
                    }
                }

                for (let i = 0; i < swiper.slides.length; i++) {
                    swiper.slides[i].classList.remove(
                'animated-left-out-next', 'animated-right-out-next',
                        'animated-left-in-next', 'animated-right-in-next',
                        'animated-left-in-prev', 'animated-right-in-prev',
                        'animated-left-out-prev', 'animated-right-out-prev'
                    );
                }
            });
        }
    }, [swiper]);

    useEffect(() => {
        const lightBox = document.querySelector('.ReactModalPortal');

        lightBox && aboutGallery.length === 1 && lightBox.classList.add('no-controls-light-box');

        if (isOpenLightBox) {
            disablePageScroll()
        } else {
            enablePageScroll()
        }
    }, [isOpenLightBox]);

    useEffect(() => {
        if (swiper !== null) {
            if (window.matchMedia("(min-width: 1366px)").matches) {
                const visibleSlideSlides = swiper.visibleSlides;
                let prevSlideLeft, prevSlideRight;

                if (swiper.activeIndex > swiper.previousIndex) {
                    if (visibleSlideSlides[0].previousSibling) {
                        prevSlideLeft = visibleSlideSlides[0].previousSibling && visibleSlideSlides[0].previousSibling.previousSibling;
                        prevSlideRight = visibleSlideSlides[0].previousSibling && visibleSlideSlides[0].previousSibling;
                    }

                    prevSlideLeft && prevSlideLeft.classList.add('animated-left-out-next');
                    prevSlideRight && prevSlideRight.classList.add('animated-right-out-next');

                    visibleSlideSlides[0].classList.add('animated-left-in-next');
                    visibleSlideSlides[1] && visibleSlideSlides[1].classList.add('animated-right-in-next');

                } else {
                    if (visibleSlideSlides[1].nextSibling) {
                        prevSlideLeft = visibleSlideSlides[1].nextSibling && visibleSlideSlides[1].nextSibling;
                        prevSlideRight = visibleSlideSlides[1].nextSibling && visibleSlideSlides[1].nextSibling.nextSibling;
                    }

                    prevSlideLeft && prevSlideLeft.classList.add('animated-left-out-prev');
                    prevSlideRight && prevSlideRight.classList.add('animated-right-out-prev');

                    visibleSlideSlides[0].classList.add('animated-left-in-prev');
                    visibleSlideSlides[1] && visibleSlideSlides[1].classList.add('animated-right-in-prev');
                }
            }
        }
    }, [slideIndexCurrent]);

    aboutGallery.length === 1 && cls["gallery-slider"].push('single-img');

    return (
        <div className={cls["gallery-slider"].join(' ')}>
            <Swiper
                {...initialSettingsSlider}
                getSwiper={updateSwiper}>
                {aboutGallery.map((slide, index) => {
                    if (index % 2 === 0) {
                        return (
                            <div key={index} className="slider-leaf slider-leaf-left">
                                <div
                                    className="wrapper-img"
                                    onClick={() => handleClickSliderLeaf(index)}>
                                    <div className="wrap">
                                        <img
                                            className="img"
                                            src={slide.thumbnailImage.url}
                                            alt={slide.thumbnailImage.alt}/>
                                    </div>
                                </div>
                                <p className="text">{slide.text} </p>
                            </div>
                        )
                    } else {
                        return (
                            <div key={index} className="slider-leaf slider-leaf-right">
                                <p className="text">{slide.text}</p>
                                <div
                                    className="wrapper-img"
                                    onClick={() => handleClickSliderLeaf(index)}>
                                    <div className="wrap">
                                        <img
                                            className="img"
                                            src={slide.thumbnailImage.url}
                                            alt={slide.thumbnailImage.alt}/>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                })}
            </Swiper>

            {isOpenLightBox && (
                <Lightbox
                    mainSrc={aboutGallery[photoIndex].originalImage.url}
                    nextSrc={aboutGallery[(photoIndex + 1) % aboutGallery.length].originalImage.url}
                    prevSrc={aboutGallery[(photoIndex + aboutGallery.length - 1) % aboutGallery.length].originalImage.url}
                    onCloseRequest={() => setOpenLightBox(false)}
                    onMovePrevRequest={() => setPhotoIndex((photoIndex + aboutGallery.length - 1) % aboutGallery.length)}
                    onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % aboutGallery.length)}
                    imageTitle={aboutGallery[photoIndex].text}
                    nextLabel={'Следущая картинка'}
                    prevLabel={'Предыдущая картинка'}
                />
            )}
        </div>
    )
};