import React, {useEffect, useRef} from 'react';
import { enablePageScroll, disablePageScroll } from 'scroll-lock';
import {BurgerButton} from "./BurgerButton";
import LogoOutline from "../svg/logo-wth-outline.svg";
import IconPhone from "../svg/icon-phone.svg";
import {ListLinksNavigation} from "./ListLinksNavigation";
import {useSelector, useDispatch} from "react-redux";
import Link from 'next/link';
import {useRouter} from 'next/router';
import {toggleHeaderMenu} from "../redux/actions/actionCreators";

export const Header = props => {
  const {
    city,
    phone,
    sectionsVisible
  } = props;

  const pageHeaderRef = useRef(null);

  const router = useRouter();
  const dispatch = useDispatch();

  const classes = {
    headerMenu: ['header__menu']
  };

  const {isOpen} = useSelector(state => state.headerMenu);

  const handleClickOverlay = (e) => {
    if (e.target.classList.contains('overlay-header-menu')) {
      dispatch(toggleHeaderMenu(false))
    }
  };

  useEffect(() => {
    if (process.browser && pageHeaderRef.current && window.innerWidth < 1700) {
      isOpen && pageHeaderRef.current.classList.add('show-menu');
      !isOpen && pageHeaderRef.current.classList.remove('show-menu');
    }

    if (isOpen) {
      disablePageScroll();
    } else {
      enablePageScroll();
    }
  }, [isOpen]);

  return (
    <header className="page-header" ref={pageHeaderRef}>
      <BurgerButton />

      {router.pathname === '/'
        ? <a className="logo"><LogoOutline /></a>
        : <Link href="/"><a className="logo"><LogoOutline /></a></Link>}


      <div className="wrapper-tel">
        <span className="city">{city}</span>
        {city && phone && <span className="divider"> </span>}
        <a href={`tel:${phone}`}>
          <IconPhone className="tel-icon"/>
          <span className="tel">{phone}</span>
        </a>
      </div>

      <div
        onClick={(e) => handleClickOverlay(e)}
        className='overlay-header-menu'>
        <ListLinksNavigation
          classes={classes.headerMenu}
          sectionsVisible={sectionsVisible} />
      </div>
    </header>
  )
};