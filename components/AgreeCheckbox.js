import React from 'react';

export const AgreeCheckbox = (
    {
        field: {name, value, onChange, onBlur},
        form: {errors, touched},
        id,
        ...props
    }
    ) =>
    {
    return (
        <label htmlFor={id} className={
            errors[name] && touched[name] ? 'label-checkbox label-checkbox_error' : 'label-checkbox'
        }>
            <input
                id={id}
                name={name}
                value={value}
                checked={value}
                type="checkbox"
                onChange={onChange}
                onBlur={onBlur}
                className="input-checkbox"
            />
            <span className="square"> </span>
            <span className="text">Я согласен с обработкой персональных данных</span>
        </label>
    );
};