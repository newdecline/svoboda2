import React from 'react';
import { useSelector, useDispatch  } from 'react-redux';
import {toggleHeaderMenu} from "../redux/actions/actionCreators";

export const BurgerButton = () => {
    const {isOpen} = useSelector(state => state.headerMenu);
    const dispatch = useDispatch();

    return (
        <button
            onClick={() => dispatch(toggleHeaderMenu(!isOpen))}
            className={isOpen ? "burger burger_active" : "burger"}>
            <span className="burger__item"> </span>
            <span className="burger__item"> </span>
            <span className="burger__item"> </span>
        </button>
    );
};