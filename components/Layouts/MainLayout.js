import React, { useEffect } from "react";
import {useRouter} from "next/router";
import {Header} from "../Header";
import {Footer} from "../Sections/Footer";
import Head from "next/dist/next-server/lib/head";
import parse from 'html-react-parser';

export const MainLayout = props => {
  const {
    children,
    layoutData: {
      city,
      phone,
      address,
      instagram_url: instagramUrl,
      vk_url: vkUrl,
      email,
      copyright,
      customCode,
      homePage: {
        show_about_section,
        show_events_section,
        show_rent_section,
        show_companies_section,
        areas
      }
    }
  } = props;

  const sectionsVisible = {
    show_about_section,
    show_events_section,
    show_rent_section,
    show_companies_section,
    show_areas_section: !!areas.length
  };

  const router = useRouter();

  const handleRouteChange = url => {
    if (history.state.as !== url) {
      typeof Ya === 'function' && Ya.Metrika2.counters().forEach(({id}) => {
        ym(id, 'hit', url);
      });

      typeof ga === 'function' && ga.getAll().forEach(item => {
        gtag('config', item.b.data.values[':trackingId'], {page_path: url});
      });
    }
  };

  useEffect(() => {
    router.events.on('beforeHistoryChange', handleRouteChange);

    return () => {
      router.events.off('beforeHistoryChange', handleRouteChange);
    }
  }, []);

  return (
    <>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" type="image/x-icon" href="/assets/favicon.ico" />
        {customCode.head && parse(`${customCode.head}`)}
      </Head>

      <Header
          city={city}
          phone={phone}
          sectionsVisible={sectionsVisible}
      />

      {children}

      <Footer
        address={[city, address]}
        socialLinks={[instagramUrl, vkUrl]}
        email={email}
        phone={phone}
        copyright={copyright}
        customCode={customCode.footer}
        sectionsVisible={sectionsVisible}
      />
    </>
  )
};