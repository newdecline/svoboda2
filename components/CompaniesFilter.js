import React, {useState} from "react";
import Collapsible from 'react-collapsible';
import classNames from 'classnames';
import ListIcon from '../svg/list.svg';
import TileIcon from '../svg/tile.svg';
import ArrowIcon from '../svg/arrow.svg';
import scrollIntoView from 'scroll-into-view';

export const CompaniesFilter = props => {
  const {
    onChangeFilter,
    onChangeDisplayType,
    displayType,
    categoryActive,
    categoriesList
  } = props;

  const [open, setOpen] = useState(false);

  const onOpenFilter = () => {
    setOpen(!open);
  };

  const handleClickDisplayTypeButton = type => {
    onChangeDisplayType(type)
  };

  const handleClickFilterItem = value => {
    const $targetScrollTag = document.querySelector(`[data-category-id="${value}"]`);

    setOpen(false);
    onChangeFilter(value);

    scrollIntoView($targetScrollTag, {
      align:{
        top: 0,
        left: 0,
        topOffset: 100,
        leftOffset: 0,
      },
    });
  };

  return <div className="filter">
    <div className={classNames("filter-list-overlay", {'open': open})}>
    <Collapsible
      handleTriggerClick={onOpenFilter}
      open={open}
      trigger={<TriggerOpen open={open}/>}
      triggerSibling={() => <TriggerSiblingOpen
        onChangeDisplayType={handleClickDisplayTypeButton}
        displayType={displayType}
        open={open}/>}
    >
        <ul className='filter-list'>
          {categoriesList.map((item, i) => {
            return (
              <li
                key={i}
                onClick={() => handleClickFilterItem(item.value)}
                className={classNames('filter-list__item', {'active': item.value === categoryActive})}
              ><span>{item.label}</span></li>
            )
          })}
        </ul>
    </Collapsible>
    </div>
  </div>
};

const TriggerSiblingOpen = ({
                              onChangeDisplayType,
                              open,
                              displayType
}) => <div className={classNames("display-type", {'disabled': open})}>
  <button
  onClick={() => onChangeDisplayType('list')}
    className={classNames("display-type__item",
      {'active': displayType === 'list'}
      )}><ListIcon/></button>
  <button
  onClick={() => onChangeDisplayType('tile')}
    className={classNames("display-type__item",
      {'active': displayType === 'tile'}
      )}><TileIcon/></button>
</div>;

const TriggerOpen = ({open}) => {
  return (
    <div className="filter__header">
      <button
        onClick={() => {}}
        className={classNames("filter-open-button", {'open': open})}
      >Все жители <span><ArrowIcon/></span></button>
    </div>
  )
};