import React, {useState, useEffect, useRef} from 'react';
import {fetchApi} from "../services/api/fetchApi";
import CheckIcon from "../svg/check.svg";
import {Formik} from "formik";
import * as Yup from "yup";
import TextareaAutosize from "react-autosize-textarea";
import elementResizeEvent from 'element-resize-event';
import {disableBodyScroll, enableBodyScroll} from 'body-scroll-lock';

export const BookingForm = props => {
    const formRef = useRef(null);

    const FormSchema = Yup.object().shape({
        name: Yup.string().required(),
        contact: Yup.string().required(),
        consent: Yup.bool().oneOf([true])
    });

    const {isFormShow, areaForm, id} = props;

    const [isFormSubmitted, setFormSubmitted] = useState(false);

    const onResizeForm = (width) => {
        const btnCloseForm = document.querySelector(`section[data-id="${id}"] .button-close`);

        if (width !== 0) {
            btnCloseForm.style.right = `${width + 1}px`;
        } else {
            btnCloseForm.style.right =  0;
        }
    };

    useEffect(() => {
        if (isFormShow) {
            disableBodyScroll(
                document.querySelector(`section[data-id="${id}"] .booking-form-wrapper`),
                {reserveScrollBarGap: true}
            );
        } else {
            enableBodyScroll(
                document.querySelector(`section[data-id="${id}"] .booking-form-wrapper`)
            );
        }
    }, [isFormShow]);

    useEffect(() => {
        const mq = window.matchMedia( "(min-width: 1024px)" );

        if (mq.matches) {
            if (formRef.current) {
                elementResizeEvent(formRef.current, (e) => {
                    if (e.currentTarget) {
                        onResizeForm(formRef.current.offsetWidth - e.currentTarget.innerWidth);
                    }
                });
            }
        }

    }, []);


    return (
        <div
            ref={formRef}
            className={isFormSubmitted ? 'booking-form-wrapper submitted' : 'booking-form-wrapper'}>
            {
                isFormSubmitted
                    ? SuccessMessage()
                    :
                    <>
                        <div className="booking-form__header">{areaForm.text}</div>

                        <Formik
                            initialValues={{
                                name: '',
                                contact: '',
                                eventDate: '',
                                message: '',
                                consent: false,
                                areaId: id
                            }}
                            onSubmit={(values) => {
                                fetchApi(`/feedback-forms/${areaForm.usageId}/use`, {
                                    method: 'POST',
                                    body: JSON.stringify(values)
                                }).then(() => {
                                    setFormSubmitted(true);
                                }).catch(() => {
                                    alert('Что-то пошло не так!');
                                });
                            }}
                            validationSchema={FormSchema}>
                            {props => {
                                const {
                                    values,
                                    touched,
                                    errors,
                                    isSubmitting,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                } = props;

                                return (
                                    <form className="form" onSubmit={handleSubmit}>
                                        <label
                                            className={errors.name && touched.name
                                                ? 'label-input label-input_error'
                                                : 'label-input'}>
                                            <input
                                                name='name'
                                                type='text'
                                                className="input"
                                                placeholder='Ваше имя *'
                                                value={values.name}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                            />
                                        </label>

                                        <label
                                            className={errors.contact && touched.contact
                                                ? 'label-input label-input_error'
                                                : 'label-input'}>
                                            <input
                                                name='contact'
                                                type='text'
                                                className="input"
                                                placeholder='Ваша почта или телефон *'
                                                value={values.contact}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                            />
                                        </label>

                                        <label
                                            className='label-input'>
                                            <input
                                                name='eventDate'
                                                type='text'
                                                className="input"
                                                placeholder='Дата мероприятия'
                                                value={values.eventDate}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                            />
                                        </label>

                                        <TextareaAutosize
                                            rows={4}
                                            className="input textarea"
                                            name="message"
                                            placeholder="Краткая информация о мероприятии, пожелания или трбования по площадке для проведения, оборудованию и т.д."
                                            value={values.message}
                                            onChange={handleChange}
                                            onBlur={handleBlur}/>

                                        <div className="wrapper-btn">
                                            <label className={
                                                errors.consent && touched.consent
                                                    ? 'label-checkbox label-checkbox_error'
                                                    : 'label-checkbox'
                                            }>
                                                <input
                                                    name='consent'
                                                    value={values.consent}
                                                    checked={values.consent}
                                                    type="checkbox"
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    className="input-checkbox"
                                                />
                                                <span className="square"><CheckIcon /></span>
                                                <span className="text">Я согласен с обработкой персональных данных</span>
                                            </label>

                                            <button
                                                className="btn btn_send"
                                                type="submit"
                                                disabled={isSubmitting}
                                            >
                                                Отправить
                                            </button>
                                        </div>
                                    </form>
                                )
                            }}
                        </Formik>
                    </>
            }
        </div>
    )
};

const SuccessMessage = () => {
    return (
       <div className="success-message-wrapper">
           <p className="success">Благодарим <br/>за&#160;обращение
               <span className="success__sub">Мы свяжемся с вами в ближайшее время.</span>
           </p>
       </div>
    )
};