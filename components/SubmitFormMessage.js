import React from 'react';

const SubmitFormMessage = props => {
    return (
        <p>
            {props.message}
        </p>
    )
};

export default SubmitFormMessage;