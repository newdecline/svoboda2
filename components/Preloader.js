import React from 'react';

export const Preloader = (props) => {
    return (
        <div className="cssload-container">
            <div className="cssload-tube-tunnel"> </div>
        </div>
    );
};