import React, {useState, useEffect, useRef} from 'react';
import { Formik } from 'formik';
import * as Yup from "yup";
import {fetchApi} from '../services/api/fetchApi';

import CheckIcon from '../svg/check.svg';
import SubmitFormMessage from './SubmitFormMessage';

const FormSchema = Yup.object().shape({
    name: Yup.string().required(),
    contact: Yup.string().required(),
    consent: Yup.bool().oneOf([true]),
});

export const FeedbackForm = props => {
    const textarea = useRef(null);
    const {feedbackForm: {usageId}, _mapInstance} = props;
    const [isSubmitForm, setSubmitForm] = useState(false);

    const handleInputTextarea = () => {
        let observe;
        if (window.attachEvent) {
            observe = function (element, event, handler) {
                element.attachEvent('on'+event, handler);
            };
        } else {
            observe = function (element, event, handler) {
                element.addEventListener(event, handler, false);
            };
        }

        if (!textarea.current) {
            return;
        }
        function resize () {
            textarea.current.style.height = 'auto';
            textarea.current.style.height = textarea.current.scrollHeight+'px';
            if (_mapInstance) {
                setTimeout(() => {_mapInstance && _mapInstance[0].container.fitToViewport()}, 250)
            }
        }
        /* 0-timeout to get the already changed text */
        function delayedResize () {
            window.setTimeout(resize, 0);
        }
        observe(textarea.current, 'change',  resize);
        observe(textarea.current, 'cut',     delayedResize);
        observe(textarea.current, 'paste',   delayedResize);
        observe(textarea.current, 'drop',    delayedResize);
        observe(textarea.current, 'keydown', delayedResize);
        observe(textarea.current, 'input', delayedResize);

        // text.focus();
        // text.select();
        textarea.current.style.height = 'auto';
        textarea.current.style.height = textarea.current.scrollHeight+'px';

    };

    useEffect(() => {
        handleInputTextarea();
    });

    if (isSubmitForm) {
        return <SubmitFormMessage message={'Форма успешно отправлена, скоро с вами свяжемся!'} />
    }

    return (
        <Formik
            initialValues={{
                name: '',
                contact: '',
                message: '',
                consent: false,
            }}
            onSubmit={(values) => {
                fetchApi(`/feedback-forms/${usageId}/use`, {
                    method: 'POST',
                    body: JSON.stringify(values)
                }).then(() => {
                    setSubmitForm(true);
                }).catch(() => {
                    alert('Что-то пошло не так!');
                });
            }}
            validationSchema={FormSchema}>
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                } = props;
                return (
                    <form className="form" onSubmit={handleSubmit}>
                        <label
                            className={errors.name && touched.name ?
                                'label-input label-input_error'
                                : 'label-input'}>
                            <input
                                name='name'
                                type='text'
                                className="input"
                                placeholder='Ваше имя *'
                                value={values.name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </label>

                        <label
                            className={errors.contact && touched.contact ?
                                'label-input label-input_error'
                                : 'label-input'}>
                            <input
                                name='contact'
                                type='text'
                                className="input"
                                placeholder='Ваша почта или телефон *'
                                value={values.contact}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </label>

                        <textarea
                            ref={textarea}
                            name="message"
                            className="input textarea"
                            placeholder="Сообщение"
                            value={values.message}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            rows="1"
                        />

                        <div className="wrapper-btn">
                            <label className={
                                errors.consent && touched.consent ? 'label-checkbox label-checkbox_error' : 'label-checkbox'
                            }>
                                <input
                                    name='consent'
                                    value={values.consent}
                                    checked={values.consent}
                                    type="checkbox"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className="input-checkbox"
                                />
                                <span className="square"><CheckIcon /></span>
                                <span className="text">Я согласен с обработкой персональных данных</span>
                            </label>

                            <button
                                className="btn btn_send"
                                type="submit"
                                disabled={isSubmitting}
                            >
                                Отправить
                            </button>
                        </div>
                    </form>
                )
            }}
        </Formik>
    );
};