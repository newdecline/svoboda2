import React, { useEffect, useState } from 'react';

export const VisibleByScroll = props => {
    const {children, onReachedBottom} = props;
    const [isReachBottom, setReachBottom] = useState(false);

    let target = children.ref.current;

    const start = () => {
        if (target) {
            const targetPosition = {
                bottom: window.pageYOffset + target.getBoundingClientRect().bottom
            };

            const windowPosition = {
                bottom: window.pageYOffset + document.documentElement.clientHeight
            };

            if (target.getBoundingClientRect().height > 100 && targetPosition.bottom < windowPosition.bottom + 1000) {
                setReachBottom(true)
            } else {
                setReachBottom(false)
            }
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', start);
    }, [children.ref.current]);

    useEffect(() => {
        onReachedBottom(isReachBottom)
    }, [isReachBottom]);

    useEffect(() => {
        return () => window.removeEventListener('scroll', start);
    }, []);

    return (children)
};