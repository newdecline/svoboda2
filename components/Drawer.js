import React, {useState, useEffect, useRef} from 'react';
import CrossIcon from "../svg/cross.svg";

export const Drawer = props => {
    const drawerContentRef = useRef(null);
    const drawerRef = useRef(null);

    const {children, open, onClose} = props;

    const [classes, setClasses] = useState({
        'drawer': ['drawer']
    });

    const onAnimationOverlayEnd = () => {
        if (classes.drawer.includes('drawer_hide')) {
            setClasses({...classes, drawer: ['drawer']});
        }
    };

    const handleClickDrawer = e => {
        if (e.target.classList.contains('drawer__content')) {
            onClose(false);
        }
    };

    useEffect(() => {
        if (open) {
            setClasses({...classes, drawer: ['drawer', 'drawer_show']});
        } else {

            if (classes.drawer.includes('drawer_show')) {
                setClasses({...classes, drawer: ['drawer', 'drawer_hide']});
            }
        }
    }, [open]);

    return (
            <div
                ref={drawerRef}
                onClick={(e) => handleClickDrawer(e)}
                className={classes.drawer.join(' ')}>
                <button className="button-close" onClick={() => onClose(false)}><CrossIcon/></button>

                <div className='drawer__overlay' onAnimationEnd={onAnimationOverlayEnd}>
                    <div className='drawer__content' ref={drawerContentRef} >
                        {children}
                    </div>
                </div>
            </div>
    )
};