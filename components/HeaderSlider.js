import React, {useState, useRef, useEffect} from 'react';
import Swiper from 'react-id-swiper';
import ArrowIcon from '../svg/arrow.svg';

export const HeaderSlider = props => {
    const headerSlider = useRef(null);
    const progressSlider = useRef(null);

    const {bannerSlides, isLoadFonts, onChangeBannerSlider} = props;

    const [gallerySlider, updateGallerySlider] = useState(null);
    const [textSlider, updateTextSlider] = useState(null);

    const [slideIndexCurrent, setSlideIndexCurrent] = useState(0);

    const goNext = () => {
        if (gallerySlider !== null) {
            gallerySlider.slideNext();
        }
    };

    const goPrev = () => {
        if (gallerySlider !== null) {
            gallerySlider.slidePrev();
        }
    };

    let initSettingsGallerySlider = {
        loop: true,
        speed: 300,
        slidesPerView: 1,
        breakpoints: {
            1024: {
                speed: 1000
            }
        }
    };

    const initSettingsTextSlider = {
        loop: true,
        loopedSlides: 1,
        speed: 1000,
        slidesPerView: 1
    };

    useEffect(() => {
        if (gallerySlider !== null) {
            gallerySlider.update();

            gallerySlider.on('slideChangeTransitionStart', () => {
                setSlideIndexCurrent(gallerySlider.realIndex);

                if (gallerySlider.previousTranslate * -1 < gallerySlider.translate * -1) {
                    textSlider.slideNext(1000);
                    onChangeBannerSlider('next')
                } else {
                    textSlider.slidePrev(1000);
                    onChangeBannerSlider('back')
                }
            });
        }
    }, [gallerySlider]);

    useEffect(() => {
        if (window.matchMedia( "(max-width: 1023px)" ).matches) {
            if (headerSlider.current && progressSlider.current) {
                const realVh = document.documentElement.clientHeight;

                headerSlider.current.style.minHeight = `${realVh}px`;
                progressSlider.current.style.top = `${realVh - 100}px`
            }
        }
    }, []);

    return (
        <>
            <div
                ref={headerSlider}
                className="slider-header">
                {bannerSlides.length !==0 &&
                    <Swiper
                        {...initSettingsGallerySlider}
                        getSwiper={updateGallerySlider}>
                        {bannerSlides.map((slide, index) => {
                            return (
                                (
                                    <div key={index} className="slider__leaf-wrapper" >
                                        <div
                                            className="slider__leaf"
                                            style={{
                                                backgroundImage: `url(${slide.image.url})`
                                            }}
                                        >
                                            <div className="slider__leaf-overlay"> </div>
                                        </div>
                                    </div>
                                )
                            )
                        })}
                    </Swiper>
                }
            </div>
            {
                bannerSlides.length <= 1
                    ? null
                    : <>
                        <button className="swiper-button-prev" onClick={goPrev}><ArrowIcon/></button>
                        <button className="swiper-button-next" onClick={goNext}><ArrowIcon/></button>

                        <div
                            ref={progressSlider}
                            className="slider-progress">
                            <span className="slider-progress__start">01.</span>
                            <span className="slider-progress__fill-overlay">
                                <span
                                    style={{
                                        width: `${(slideIndexCurrent + 1) / bannerSlides.length * 100}%`
                                    }}
                                    className="slider-progress__fill-active"
                                    > </span>
                            </span>
                            <span className="slider-progress__end">0{bannerSlides.length}.</span>
                        </div>
                    </>
            }

            {bannerSlides.length !== 0 &&
                <div
                    className={isLoadFonts
                    ? "slider-text once-start-animate"
                    : "slider-text"}>
                    <Swiper
                        {...initSettingsTextSlider}
                        getSwiper={updateTextSlider}>
                        {bannerSlides.map((slide, index) => (
                            <div
                                key={index}
                                className="slider-text__leaf">
                                <h2 className="title">{slide.title}</h2>
                            </div>
                        ))}
                    </Swiper>
                </div>
            }
        </>
    );
};