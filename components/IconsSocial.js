import React from 'react';
import IconInstagram from '../svg/instagram-icon.svg';
import IconVk from '../svg/vk-icon.svg';

const iconsList = [<IconInstagram/>, <IconVk/>];

export const IconsSocial = props => {
	const {socialLinks} = props;

    return (
        <ul className="list-social">
			{
				socialLinks.map((socialLink, index) => {
					if (socialLink) {
						return (
							<li key={index} className="list-social__link">
								<a href={socialLink} target="_blank">
									{iconsList[index]}
								</a>
							</li>
						)
					}
				})
			}
        </ul>
    )
};