import React from "react";
import {GallerySlider} from "../GallerySlider";

export const AboutSection = props => {
    const {aboutSectionHeader, aboutText, aboutGallery} = props;

    return (
        <div id="svoboda2">
            <section className={aboutGallery.gallerySlides.length ? "about" : "about no-carousel"}>
                <div className="vertical-line-wrapper">
                    <div className="line"> </div>
                    <div className="line"> </div>
                    <div className="line"> </div>
                </div>

                <div className="about__col-left">
                    <h2 className="title">{aboutSectionHeader}</h2>
                    <p className="subtitle">
                        {aboutText}
                    </p>
                </div>

                { aboutGallery.gallerySlides.length
                    ? <GallerySlider aboutGallery={aboutGallery.gallerySlides} />
                    : null }
            </section>
        </div>
    );
};