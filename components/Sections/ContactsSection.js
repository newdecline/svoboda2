import React, {useState, useEffect, useRef} from 'react';
import { YMaps, Map, Placemark  } from 'react-yandex-maps';
import {FeedbackForm} from "../FeedbackForm";
import elementResizeEvent from "element-resize-event";

export const ContactsSection = props => {
    const {
        contactsSectionHeader,
        contactsCoordinates,
        contactsAddress,
        contactsPhone1,
        contactsPhone2,
        contactsEmail,
        feedbackForm
    } = props;

    let mapInstance;
    const contactsMap = useRef(null);
    const contactsBlock = useRef(null);

    const [isMapOpen, setMapOpen] = useState(false);
    const [isMapLoaded, setMapLoaded] = useState(false);
    const [_mapInstance, setInstanceMap] = useState(false);
    const [mapData, setMapData] = useState({center: [], zoom: 17});

    const offsetCenterCard = (offsetX, offsetY, zoom = 17) => {
        if (mapInstance) {
            const position = mapInstance[0].getGlobalPixelCenter();
            mapInstance[0].setGlobalPixelCenter([ position[0] + offsetX, position[1] + offsetY ], zoom);
            mapInstance[0].behaviors.disable('scrollZoom');
        }
    };

    const loadedCard = () => {
        setMapLoaded(!isMapLoaded);
        setInstanceMap(mapInstance);

        if (window.matchMedia( "(max-width: 1023px)" ).matches || window.matchMedia( "(min-width: 1366px)" ).matches) {
            offsetCenterCard(0, -160, 17);
        }

        if (window.innerWidth >= 1024) {
            offsetCenterCard(0, 0, 17);
        }
    };

    const toggleOpenMap = () => {
        setMapOpen(!isMapOpen);
    };

    const handleFitToViewport = () => {
        isMapLoaded && mapInstance[0].container.fitToViewport();
    };

    const handleResizeContactsBlock = (element) => {
        if (window.matchMedia( "(min-width: 1366px)" ).matches) {
            contactsMap.current.style.height = `${element.offsetHeight}px`;
        }
    };

    useEffect(() => {
		contactsCoordinates && setMapData({center: JSON.parse(contactsCoordinates), zoom: 17});
        elementResizeEvent(contactsBlock.current, () => handleResizeContactsBlock(contactsBlock.current));
        window.addEventListener('resize', handleFitToViewport);
        return () => {
            window.removeEventListener('resize', handleFitToViewport);
        }
    }, []);

    useEffect(() => {
        isMapOpen ? offsetCenterCard(0, -160, 17) : offsetCenterCard(0, 160, 17);
    });


    return (
        <div id="kontakty">
            <div className={isMapOpen ? "contacts contacts_open-map" : "contacts"}>
                <YMaps>
                    <div
                        ref={contactsMap}
                        id="map"
                        className="map">
                        <div
                            onClick={toggleOpenMap}
                            className="boost-map"> </div>
                        <Map
                            defaultState={mapData}
                            width="100%" height="100%"
                            instanceRef={(...map) => mapInstance = map}
                            onLoad={() => loadedCard()}
                        >
                            <Placemark
                                geometry={mapData.center}
                                options={{
                                    iconLayout: 'default#image',
                                    iconImageHref: '/assets/location-pin.svg',
                                    iconImageSize: [80, 86],
                                    iconImageOffset: [-80, -86]
                                }}
                            />
                        </Map>
                    </div>
                </YMaps>

                <div ref={contactsBlock} className="contacts-block">

                    <div className="wrapper">
                        <h2 className="title">{contactsSectionHeader}</h2>
                        <div className="address">
                            <span>{contactsAddress}</span>
                            <a href={`tel:${contactsPhone1}`}>{contactsPhone1}</a>
                            <a href={`tel:${contactsPhone2}`}>{contactsPhone2}</a>
                        </div>
                        <div className="contacts-block-mail">
                            <a href={`mailto:${contactsEmail}`}>{contactsEmail}</a>
                        </div>
                    </div>

                    <div className="wrapper">
                        <div className="message">{`${feedbackForm.text || ''}`}</div>
                        <FeedbackForm feedbackForm={feedbackForm} _mapInstance={_mapInstance}/>
                    </div>

                </div>
            </div>
        </div>
    )
};