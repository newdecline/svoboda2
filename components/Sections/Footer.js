import React from 'react';
import parse from 'html-react-parser';
import LogoFooter from '../../svg/logo.svg';
import {IconsSocial} from "../IconsSocial";
import {ListLinksNavigation} from "../ListLinksNavigation";

export const Footer = props => {
  const {
    address,
    email,
    socialLinks,
    copyright,
    phone,
    customCode,
    sectionsVisible
  } = props;

  return (
    <>
      <footer className="page-footer">
        <div className="page-footer__column">
          <div className="copyright">Все права защищены <br/>
            {copyright}
          </div>
        </div>

        <div className="page-footer__column">
          <div className="address">{address.map((address => {
            return <span key={address}>{address}</span>
          }))}</div>

          <a href={`tel:${phone}`} className="tel">{phone}</a>

          <a href={`mailto:${email}`} className="page-footer-mail">{email}</a>

          <IconsSocial socialLinks={socialLinks}/>
        </div>

        <div className="page-footer__column">
          <ListLinksNavigation
            classes={['page-footer__menu']}
            sectionsVisible={sectionsVisible}
          />
        </div>

        <div className="page-footer__column">
          <a href="http://pinkchilli.agency" className="logo-footer" target="_blank">
            <span>Сайт разработан</span>
            <LogoFooter/>
          </a>

          <div className="developer-date">2019 г.</div>
        </div>
      </footer>

      {customCode && <div className='custom-code'> {parse(`${customCode}`)}</div>}

    </>
  );
};