import React from 'react';

export const HoldEventSection = props => {
    const {ownEventImage, ownEventSectionHeader, ownEventDescriptions} = props;
   
    return (
        <section id='provedi-meropriyatie' className="hold-event">
            <div className="wrapper-text">
                <h2 className='hold-event__header'>{ownEventSectionHeader}</h2>
                <p className='hold-event__description'>{ownEventDescriptions}</p>
            </div>

            <div
                style={{backgroundImage: `url(${ownEventImage.url}`}}
                className="hold-event__img">
            </div>
        </section>
    )
};