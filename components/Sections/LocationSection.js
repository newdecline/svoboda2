import React, {useState} from 'react';
import {GallerySlider} from "../GallerySlider";
import WindowsIcon from '../../svg/windows.svg';
import TeamIcon from '../../svg/team.svg';
import MoneyIcon from '../../svg/money.svg';
import PhoneIcon from '../../svg/phone.svg';
import {BookingForm} from "../BookingForm";
import {Drawer} from '../Drawer';

export const LocationSection = props => {
    const [isFormShow, setFormShow] = useState(false);

    const {
        areaForm,
        bullet_capacity,
        bullet_contact,
        bullet_description,
        bullet_rent,
        gallery,
        name,
        id
    } = props;

    return (
        <section className='location' data-id={id}>
            <div className="d-flex">
                <div className="vertical-line-wrapper">
                    <div className="line"> </div>
                    <div className="line"> </div>
                    <div className="line"> </div>
                </div>

                <div className="location__characteristics">
                    <h2 className='location__header'>{name}</h2>

                    <ul className="characteristics-list">
                        {bullet_description && <li className="item">
                            <span className='item__icon'><WindowsIcon/></span>
                            <p className='item__text'>{bullet_description}</p>
                        </li>}
                        {bullet_capacity && <li className="item">
                            <span className='item__icon'><TeamIcon/></span>
                            <p className='item__text'>{bullet_capacity}</p>
                        </li>}
                        {bullet_rent && <li className="item">
                            <span className='item__icon'><MoneyIcon/></span>
                            <p className='item__text'>{bullet_rent}</p>
                        </li>}
                        {bullet_contact && <li className="item">
                            <span className='item__icon'><PhoneIcon/></span>
                            <p className='item__text'>{bullet_contact}</p>
                        </li>}
                    </ul>

                    <button
                        className='btn btn-reserve'
                        onClick={() => setFormShow(true)}>
                        Забронировать</button>
                </div>

                {
                    gallery.gallerySlides.length
                        ? <GallerySlider
                            aboutGallery={gallery.gallerySlides} />
                        : null
                }
            </div>

            <Drawer
                open={isFormShow}
                onClose={setFormShow}>
                <BookingForm
                    areaForm={areaForm}
                    isFormShow={isFormShow}
                    id={id}/>
            </Drawer>

        </section>
    )
};
