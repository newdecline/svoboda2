import React, {useState, useEffect} from 'react';
import Swiper from 'react-id-swiper';
import Link from "next/link";
import ArrowIcon from "../../svg/arrow.svg";

export const EventsSection = props => {
  const {
    eventsSectionHeader,
    eventsDescriptions,
    events
  } = props;

  const [gallerySlider, updateGallerySlider] = useState(null);
  const [isBeginning, setBeginning] = useState(true);
  const [isEnd, setEnd] = useState(false);
  const [controls, setControls] = useState(true);

  const initialSettings = {
    speed: 400,
    spaceBetween: 50,
    watchSlidesProgress: true,
    watchSlidesVisibility: true,
    preventClicks: true,
    preventClicksPropagation: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        preventClicks: false,
        preventClicksPropagation: false,
      },
      1024: {
        slidesPerView: 2,
      },
      1366: {
        slidesPerView: 3,
      }
    }
  };

  const goNext = () => {
    if (gallerySlider !== null) {
      gallerySlider.slideNext();
    }
  };

  const goPrev = () => {
    if (gallerySlider !== null) {
      gallerySlider.slidePrev();
    }
  };

  useEffect(() => {
    if (gallerySlider !== null) {
      gallerySlider.update();

      setControls(events.length > gallerySlider.visibleSlides.length);

      gallerySlider.on('transitionStart', function () {
        setBeginning(gallerySlider.isBeginning);
        setEnd(gallerySlider.isEnd);
      });
    }
  }, [gallerySlider]);

  const classes = {
    'events': ['events']
  };

  events.length === 0 && classes.events.push('no-slides');
  !controls && classes.events.push('slider-no-controls');

  return (
    <section id="afisha" className={classes.events.join(' ')}>
      <h2 className='events__header'>{eventsSectionHeader}</h2>

      <div className="d-flex">
        <div className="events__description">
          <p className="text">
            {eventsDescriptions}
          </p>

          <Link href='/bill' as='/afisha'>
            <a className='all-events'>Все мероприятия</a>
          </Link>
        </div>

        {events.length !== 0 && <div
          className='events__slider'>
          <Swiper
            {...initialSettings}
            getSwiper={updateGallerySlider}>
            {
              events.map((item, index) => {
                return (
                  <div key={index}>
                    <Link href={`/event?slug=${item.slug}`} as={`/afisha/${item.slug}`}>
                      <a className='slider__item'>
                        <div className="img"><img src={item.thumbnailImage.url} alt={item.thumbnailImage.alt}/></div>
                        <div className="date">{item.date}</div>
                        <h2 className="title">{item.name}</h2>
                        <span className='link'>Подробнее</span>
                      </a>
                    </Link>
                  </div>

                )
              })
            }
          </Swiper>

          <button disabled={isBeginning} className="swiper-button-prev" onClick={goPrev}><ArrowIcon/></button>
          <button disabled={isEnd} className="swiper-button-next" onClick={goNext}><ArrowIcon/></button>
        </div>}
      </div>

    </section>
  )
};