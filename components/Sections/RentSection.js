import React, {useState, useEffect} from 'react';
import Swiper from 'react-id-swiper';
import Link from "next/link";
import ArrowIcon from "../../svg/arrow.svg";

export const RentSection = props => {
  const {
    rooms,
    rentSectionHeader,
    rentDescriptions
  } = props;

  const [gallerySlider, updateGallerySlider] = useState(null);
  const [isBeginning, setBeginning] = useState(true);
  const [isEnd, setEnd] = useState(false);
  const [controls, setControls] = useState(true);

  const initialSettings = {
    speed: 400,
    spaceBetween: 50,
    watchSlidesProgress: true,
    watchSlidesVisibility: true,
    preventClicks: true,
    preventClicksPropagation: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        preventClicks: false,
        preventClicksPropagation: false,
      },
      1024: {
        slidesPerView: 2,
      },
      1366: {
        slidesPerView: 3,
      }
    }
  };

  const goNext = () => {
    if (gallerySlider !== null) {
      gallerySlider.slideNext();
    }
  };

  const goPrev = () => {
    if (gallerySlider !== null) {
      gallerySlider.slidePrev();
    }
  };

  useEffect(() => {
    if (gallerySlider !== null) {
      gallerySlider.update();

      setControls(rooms.length > gallerySlider.visibleSlides.length);

      gallerySlider.on('transitionStart', function () {
        setBeginning(gallerySlider.isBeginning);
        setEnd(gallerySlider.isEnd);
      });
    }
  }, [gallerySlider]);

  const classes = {
    'rent': ['rent']
  };

  rooms.length === 0 && classes.rent.push('no-slides');
  !controls && classes.rent.push('slider-no-controls');

  return <section id="arenda" className={classes.rent.join(' ')}>
    <h2 className='rent__header'>{rentSectionHeader}</h2>

    <div className="d-flex">
      <div className="rent__description">
        <p className="text">
          {rentDescriptions}
        </p>

        {rooms.length !== 0 && <Link href='/rooms' as='/arenda'>
          <a className='all-rent'>Все помещения</a>
        </Link>}
      </div>

      {rooms.length !== 0 && <div
        className='rent__slider'>
        <Swiper
          {...initialSettings}
          getSwiper={updateGallerySlider}>
          {rooms.map((item, index) => {
            const cost = item.cost.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

            return (
              <div key={index}>
                <Link href={`/room?slug=${item.slug}`} as={`/arenda/${item.slug}`}>
                  <a className='slider__item'>
                    <div className="img"><img src={item.thumbnailImage.url} alt={item.thumbnailImage.alt}/></div>
                    {(item.cost || item.square) && <div className="cost">
                      {cost} руб. <span className="cost__divider"/> {item.square} кв.м.
                    </div>}
                    <h2 className="title">{item.name}</h2>
                    <span className='link'>Подробнее</span>
                  </a>
                </Link>
              </div>

            )
          })}
        </Swiper>

        <button disabled={isBeginning} className="swiper-button-prev" onClick={goPrev}><ArrowIcon/></button>
        <button disabled={isEnd} className="swiper-button-next" onClick={goNext}><ArrowIcon/></button>
      </div>}
    </div>

  </section>
};