import React from 'react';
import Link from "next/link";

export const CompaniesSection = props => {
  const {bannerImage, title, description} = props;

  return (
    <section id='zhiteli' className="companies-section">
      <div className="wrapper-text">
        <h2 className='companies-section__header'>{title}</h2>
        <p className='companies-section__description'>{description}</p>
        <Link href="/companies" as='/zhiteli'><a className='all-companies'>Все жители</a></Link>
      </div>

      <div
        style={{backgroundImage: `url(${bannerImage.url}`}}
        className="companies-section__img">
      </div>
    </section>
  )
};