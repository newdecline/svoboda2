import React from 'react';
import { toggleHeaderMenu } from "../redux/actions/actionCreators";
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import Link from 'next/link';
import scrollIntoView from 'smooth-scroll-into-view-if-needed';

export const ListLinksNavigation = props => {
  const {
    classes,
    sectionsVisible: {
      show_about_section: showAboutSection,
      show_events_section: showEventsSection,
      show_areas_section: showAreasSection,
      show_rent_section: showRentSection,
      show_companies_section: showCompaniesSection,
    }
  } = props;

  const dispatch = useDispatch();

  const router = useRouter();

  let headerLinks = [
    {
      href: 'glavnaya',
      label: 'Главная'
    },
    {
      href: 'svoboda2',
      label: 'Svoboda2'
    },
    {
      href: 'afisha',
      label: 'Афиша'
    },
    {
      href: 'provedi-meropriyatie',
      label: 'Площадки'
    },
    {
      href: 'arenda',
      label: 'Аренда'
    },
    {
      href: 'zhiteli',
      label: 'Жители'
    },
    {
      href: 'kontakty',
      label: 'Контакты'
    }
  ];

  const handleClickMenuItem = (e) => {
    e.preventDefault();
    const hash = e.target.hash;
    const target = document.querySelector(hash);

    scrollIntoView(target, {
      behavior: 'smooth',
      block: 'start',
    }).then(() => {
      router.replace(`${router.route}${hash}`, `${router.route}${hash}`, {shallow: true});
    });
  };


  return (
    <ul className={classes.join(' ')}>
      {headerLinks.map((link, index) => {
        if (!showAboutSection && link.href === 'svoboda2') {return;}
        if (!showEventsSection && link.href === 'afisha') {return;}
        if (!showAreasSection && link.href === 'ploshchadki') {return;}
        if (!showRentSection && link.href === 'arenda') {return;}
        if (!showCompaniesSection && link.href === 'zhiteli') {return;}

        if (router.pathname === '/') {

          return (
            <li
              key={index}
              className="menu-item"
              onClick={() => {
                dispatch(toggleHeaderMenu(false));
              }}
            >
              <a
                onClick={(e) => {
                  handleClickMenuItem(e);
                }}
                href={`#${link.href}`}>{link.label}</a>
            </li>
          )
        }

        return (
          <li
            key={index}
            className="menu-item"
            onClick={() => {
              dispatch(toggleHeaderMenu(false));
            }}
          >
            <Link href={`/#${link.href}`}>
              <a>{link.label}</a>
            </Link>
          </li>
        )
      })}
    </ul>
  )
};