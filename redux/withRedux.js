import React from 'react';
import App from 'next/app';
import { Provider } from 'react-redux';
import { initializeStore } from './store'
import {MainLayout} from "../components/Layouts/MainLayout";
import {fetchApi} from "../services/api/fetchApi";
import {Div100Vh} from "../hoc/Div100Vh";
import {omit, pick} from 'lodash';
import '../sass/index.scss';

export const withRedux = (
    PageComponent, { ssr = true, Layout = MainLayout, fetchURLLayout = '/layout', classes = []} = {}) => {

    const WithRedux = ({ initialReduxState, ...props }) => {
        if (PageComponent.name !== 'Error' && props.error && props.error.statusCode === 404) {
            const err = new Error();
            err.statusCode = 404;
            err.code = 'ENOENT';
            throw err;
        }

        const store = getOrInitializeStore(initialReduxState);

        return (
            <Provider store={store}>
                <Div100Vh>
                    <div className={classes.join(' ')}>
                        <Layout {...pick(props, 'layoutData')}>
                            <PageComponent {...props} />
                        </Layout>
                    </div>
                </Div100Vh>
            </Provider>
        )
    };

    if (process.env.NODE_ENV !== 'production') {
        const isAppHoc =
            PageComponent === App || PageComponent.prototype instanceof App;
        if (isAppHoc) {
            throw new Error('The withRedux HOC only works with PageComponents')
        }
    }

    if (process.env.NODE_ENV !== 'production') {
        const displayName =
            PageComponent.displayName || PageComponent.name || 'Component';

        WithRedux.displayName = `withRedux(${displayName})`
    }

    if (ssr || PageComponent.getInitialProps) {
        WithRedux.getInitialProps = async context => {
            const reduxStore = getOrInitializeStore();

            context.reduxStore = reduxStore;

            let pageProps =
                typeof PageComponent.getInitialProps === 'function'
                    ? await PageComponent.getInitialProps(context)
                    : {};

            if (fetchURLLayout) {
                return fetchApi(`${fetchURLLayout}`,{
                    query: {
                        fields: `*, 
                        homePage.show_about_section, 
                        homePage.show_events_section, 
                        homePage.show_rent_section,
                        homePage.show_companies_section`,
                        expand: 'customCode, homePage, homePage.areas'
                    }
                }).then(res => {
                    return {
                        ...pageProps,
                        layoutData: res.data,
                        initialReduxState: reduxStore.getState()
                    }}
                );
            } else {
                return {
                    ...pageProps,
                    initialReduxState: reduxStore.getState()
                }
            }
        }
    }

    return WithRedux;
};

let reduxStore;

const getOrInitializeStore = initialState => {
    if (typeof window === 'undefined') {
        return initializeStore(initialState)
    }

    if (!reduxStore) {
        reduxStore = initializeStore(initialState)
    }

    return reduxStore
};