import {actionTypes} from "../actions/actionTypes";

const initialState = {
    isOpen: false
};

export const headerMenu = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_HEADER_MENU:
            return {...state, isOpen: action.payload};
        default:
            return state
    }
};