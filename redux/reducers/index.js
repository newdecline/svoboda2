import { combineReducers } from "redux";
import { headerMenu } from './headerMenu';

export default combineReducers({
    headerMenu
});