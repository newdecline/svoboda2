import {actionTypes} from "../actions/actionTypes";

export const toggleHeaderMenu = payload => {
    return { type: actionTypes.TOGGLE_HEADER_MENU, payload }
};