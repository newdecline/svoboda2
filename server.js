const express = require('express');
const next = require('next');
const proxy = require('http-proxy-middleware');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 3000;

const optionsProxy = proxy({
    target: process.env.BASE_URL,
    changeOrigin: true
});

app.prepare().then(() => {
    const server = express();

    dev && server.use(express.static('public'));
    server.use('/api', optionsProxy);
    server.use('/images', optionsProxy);

    server.get('/', (req, res) => {
        const actualPage = '/index';
        app.render(req, res, actualPage);
    });

    server.get('/afisha', (req, res) => {
        const actualPage = '/bill';
        app.render(req, res, actualPage);
    });

    server.get('/afisha/:slug', (req, res) => {
        const actualPage = '/event';
        const queryParams = { slug: req.params.slug };
        app.render(req, res, actualPage, queryParams);
    });

    server.get('/arenda', (req, res) => {
        const actualPage = '/rooms';
        app.render(req, res, actualPage);
    });

    server.get('/arenda/:slug', (req, res) => {
        const actualPage = '/room';
        const queryParams = { slug: req.params.slug };
        app.render(req, res, actualPage, queryParams);
    });

    server.get('/zhiteli', (req, res) => {
        const actualPage = '/companies';
        app.render(req, res, actualPage);
    });

    server.get('/zhiteli/:slug', (req, res) => {
        const actualPage = '/company';
        const queryParams = { slug: req.params.slug };
        app.render(req, res, actualPage, queryParams);
    });

    server.get('*', (req, res) => handle(req, res));

    const listenCallback = err => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`)
    };

    dev ? server.listen(port, listenCallback) : server.listen(port, 'localhost', listenCallback);
    })
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
});