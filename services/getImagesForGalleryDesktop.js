export const getImagesForGalleryDesktop = (arrImages) => {
    if (!arrImages) {
        return null
    }

    const imgArrDesktop = [];

    for (let i = 0; i < arrImages.length; i++) {
        if (i === arrImages.length - 1) {
            imgArrDesktop.push([
                {
                    imgIndex: i,
                    imgSrc: arrImages[i].thumbnailImage.url,
                    text: arrImages[i].text,
                    alt: arrImages[i].thumbnailImage.alt}
                ]);
        } else {
            imgArrDesktop.push([
                {
                    imgIndex: i,
                    imgSrc: arrImages[i].thumbnailImage.url,
                    text: arrImages[i].text,
                    alt: arrImages[i].thumbnailImage.alt},
                {
                    imgIndex: i + 1,
                    imgSrc: arrImages[i + 1].thumbnailImage.url,
                    text: arrImages[i + 1].text,
                    alt: arrImages[i + 1].thumbnailImage.alt}
            ]);
            i++;
        }
    }

    return imgArrDesktop;
};