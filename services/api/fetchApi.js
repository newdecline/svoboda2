import fetch from 'isomorphic-unfetch';

export const fetchApi = async (url, customOptions = {}) => {
    const options = {
        query: {},
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
            ...customOptions.headers
        },
        ...customOptions
    };

    let urlObject = '';

    if (process.browser) {
        urlObject = new URL(`/api${url}`, window.location.origin);
    } else {
        urlObject = new URL(`/api${url}`, `${process.env.BASE_URL}`);
    }

    for (const key in options.query) {
        urlObject.searchParams.append(key, options.query[key]);
    }

    const response = await fetch(urlObject.href, options);

    return {
        headers: response.headers,
        status: response.status,
        data: await response.json()
    }
};