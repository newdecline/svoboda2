import React, {useEffect, useState} from "react";
import {withRedux} from "../redux/withRedux";
import {fetchApi} from "../services/api/fetchApi";
import ArrowIcon from "../svg/arrow.svg";
import LocationIcon from "../svg/location.svg";
import Swiper from "react-id-swiper";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import Lightbox from "react-image-lightbox";
import {Portal} from "react-portal";
import CrossIcon from "../svg/cross.svg";
import Head from "next/head";

const CompanyPage = props => {
  const {
    vk_url,
    instagram_url,
    facebook_url,
    site_url,
    name,
    location,
    locationImage,
    phone,
    description,
    address,
    email,
    bannerImage,
    renter_from,
    gallery,
    pageMeta
  } = props;

  const [photoIndex, setPhotoIndex] = useState(0);
  const [isOpenLightBox, setOpenLightBox] = useState(false);
  const [gallerySlider, updateGallerySlider] = useState(null);
  const [isBeginning, setBeginning] = useState(true);
  const [isEnd, setEnd] = useState(false);
  const [controls, setControls] = useState(true);

  const [openViewDrivingPopup, setOpenViewDrivingPopup] = useState(false);
  useEffect(() => {
    if (openViewDrivingPopup) {
      disablePageScroll();
    } else {
      enablePageScroll();
    }
  }, [openViewDrivingPopup]);

  const initialSettings = {
    speed: 400,
    slidesPerView: 1,
    watchSlidesProgress: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
  };

  const goNext = () => {
    if (gallerySlider !== null) {
      gallerySlider.slideNext();
    }
  };

  const goPrev = () => {
    if (gallerySlider !== null) {
      gallerySlider.slidePrev();
    }
  };

  const handleClickSliderLeaf = index => {
    setPhotoIndex(index);
    setOpenLightBox(true);
  };

  useEffect(() => {
    if (gallerySlider !== null) {
      if (gallerySlider.slides.length <= 1) {
        setControls(false);
        document.body.classList.add('no-controls-light-box');
      }

      gallerySlider.on('transitionStart', function () {
        setBeginning(gallerySlider.isBeginning);
        setEnd(gallerySlider.isEnd);
      });
    }
  }, [gallerySlider]);

  useEffect(() => {
    if (isOpenLightBox) {
      disablePageScroll()
    } else {
      enablePageScroll()
    }
  }, [isOpenLightBox]);

  const handleClickLocationBtn = () => {
    setOpenViewDrivingPopup(!openViewDrivingPopup);
  };

  const handleClickDrivingDirectionsPopup = e => {
    if (e.target.dataset.closePopup) {
      setOpenViewDrivingPopup(!openViewDrivingPopup);
    }
  };

  const haveSocialNetwork = () => (vk_url || instagram_url|| facebook_url);
  const haveSites = () => (site_url);

  const sites = [site_url];

  return <>
    <Head>
      <title>{pageMeta.title || ''}</title>
      <meta name="keywords" content={pageMeta.keywords || ''}/>
      <meta name="description" content={pageMeta.description || ''}/>
    </Head>

    <div className="banner" style={{backgroundImage: `url(${bannerImage.url})`}}/>

    <main className="content">
      <div className="content__text-wrapper">
        <h3 className="name">{name}</h3>

        <div className="company-location">
          <b>Расположение:&nbsp;</b>
          <span className="company-location__text" title={location}>
            {location}
            {locationImage.url && <LocationIcon onClick={handleClickLocationBtn} className='company-location__icon'/>}
          </span>
          {locationImage.url && <LocationIcon onClick={handleClickLocationBtn} className='company-location__icon'/>}
        </div>

        <div className="links-block">
          {haveSocialNetwork() && <div className='links-block__item'>
            <div className="links-block__header"><b>В соцсетях:</b></div>
            <ul>
              {vk_url && <li><a
                rel="noreferrer noopener"
                target="_blank"
                href={vk_url}
                className='link'>VK</a></li>}
              {instagram_url && <li><a
                rel="noreferrer noopener"
                target="_blank"
                href={instagram_url}
                className='link'>Instagram</a></li>}
              {facebook_url && <li><a
                rel="noreferrer noopener"
                target="_blank"
                href={facebook_url}
                className='link'>Facebook</a></li>}
            </ul>
          </div>}

          {haveSites() && <div className='links-block__item'>
            <div className="links-block__header"><b>Сайт:</b></div>
            <ul>
              {sites.map((link, i) => {
                return <li key={i}><a
                  rel="noreferrer noopener"
                  target="_blank"
                  href={link}
                  className='link'>{getDomain(link)}</a></li>
              })}
            </ul>
          </div>}
        </div>

        {description && <div className="company-text">{description}</div>}

        <div className="company-contacts-block">
          <div className="company-contacts-block-header">Контакты</div>
          <a className="company-contacts-block__item">
            <b>Адрес: </b>{address}
          </a>
          {phone && <a href={`tel:${phone}`} className="company-contacts-block__item">
            <b>Телефон: </b>{phone}
          </a>}
          {email && <a href={`mailto:${email}`} className="company-contacts-block__item">
            <b>E-mail: </b>{email}
          </a>}
        </div>

        {renter_from && <div className="check-in-date">На SVOBODA: {renter_from}</div>}
      </div>

      {!gallery.gallerySlides.length
        ? null
        : <div
          className={controls
            ? "gallery"
            : "gallery no-controls"}>
          <button disabled={isBeginning} className="swiper-button-prev" onClick={goPrev}><ArrowIcon/></button>
          <button disabled={isEnd} className="swiper-button-next" onClick={goNext}><ArrowIcon/></button>

          <Swiper
            {...initialSettings}
            getSwiper={updateGallerySlider}>
            {
              gallery.gallerySlides.map((item, index) => {
                return (
                  <div key={index} className="slider-leaf">
                    <div
                      className="wrapper-img"
                      onClick={() => handleClickSliderLeaf(index)}>
                      <img
                        className="img"
                        src={item.thumbnailImage.url}
                        alt={item.thumbnailImage.alt}
                      />
                    </div>
                  </div>
                )
              })
            }
          </Swiper>
        </div>}
    </main>

    {
      isOpenLightBox && <Lightbox
        mainSrc={gallery.gallerySlides[photoIndex].originalImage.url}
        nextSrc={gallery.gallerySlides[(photoIndex + 1) % gallery.gallerySlides.length].originalImage.url}
        prevSrc={gallery.gallerySlides[(photoIndex + gallery.gallerySlides.length - 1) % gallery.gallerySlides.length].originalImage.url}
        imageTitle={gallery.gallerySlides[photoIndex].text}
        onCloseRequest={() => setOpenLightBox(false)}
        onMovePrevRequest={() => setPhotoIndex((photoIndex + gallery.gallerySlides.length - 1) % gallery.gallerySlides.length)}
        onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % gallery.gallerySlides.length)}
        nextLabel={'Следущая картинка'}
        prevLabel={'Предыдущая картинка'}
      />
    }

    {openViewDrivingPopup && <Portal>
      <div data-close-popup={true} className="driving-directions" onClick={handleClickDrivingDirectionsPopup}>
        <button onClick={handleClickLocationBtn} className="close"><CrossIcon/></button>
        <img
          src={locationImage.url}
          alt={locationImage.alt}/>
      </div>
    </Portal>}
  </>
};

CompanyPage.getInitialProps = async (res) => {
  const {slug} = res.query;

  const companyPageData = await fetchApi(`/companies/published/${slug}`, {
      query: {
        expand: `pageMeta, 
      gallery.gallerySlides,
      bannerImage,
      avatarImage,
      locationImage,
      name, 
      location,
      vk_url,
      instagram_url,
      facebook_url,
      site_url,
      description,
      address,
      phone,
      email,
      renter_from`
      },
      fields: `pageMeta, 
      gallery.gallerySlides,
      bannerImage,
      avatarImage,
      locationImage,
      name, 
      location,
      vk_url,
      instagram_url,
      facebook_url,
      site_url,
      description,
      address,
      phone,
      email,
      renter_from`
    }
  );

  if (companyPageData.status === 404) {
    res.statusCode = 404;

    return {
      error: {
        statusCode: 404
      }
    };
  }

  return companyPageData.data;
};

export default withRedux(CompanyPage, {classes: ['company-page']});

function getHostName(url) {
  const match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
  if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
    return match[2];
  } else {
    return null;
  }
}

function getDomain(url) {
  const hostName = getHostName(url);
  let domain = hostName;

  if (hostName != null) {
    const parts = hostName.split('.').reverse();

    if (parts != null && parts.length > 1) {
      domain = parts[1] + '.' + parts[0];

      if (hostName.toLowerCase().indexOf('.co.uk') != -1 && parts.length > 2) {
        domain = parts[2] + '.' + domain;
      }
    }
  }

  return domain;
}