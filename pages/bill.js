import React, {useState, useRef} from 'react';
import {withRedux} from "../redux/withRedux";
import Link from 'next/link';
import {fetchApi} from "../services/api/fetchApi";
import Head from "next/head";
import {VisibleByScroll} from '../components/VisibleByScroll';

const Bill = props => {
  const eventsOldGrid = useRef(null);
  const eventsOld = useRef(null);

  const {
    pageMeta,
    bannerImage,
    banner_link: bannerLink,
    banner_header: bannerHeader,
    banner_sub_header: bannerSubHeader,
    events
  } = props;

  const [isArchiveVisible, setArchiveVisible] = useState(false);
  const [filterPoints, setFilterPoints] = useState([]);
  const [eventsArchive, setEventsArchive] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [maxPageCount, setMaxPageCount] = useState(1);
  const [currentYear, setCurrentYear] = useState('Все');

  const handleClickEventsArchiveBtn = () => {
    eventsOld.current && eventsOld.current.scrollIntoView({block: 'start', behavior: 'smooth'});

    if (isArchiveVisible) {
      return
    }

    setArchiveVisible(true);

    fetchApi('/events/archive/filter')
      .then(res => {
        setFilterPoints(res.data);
      })
      .catch(err => {
        console.log(err)
      });

    fetchApi('/events/archive', {
      query: {
        fields: 'name, date, slug',
        expand: 'thumbnailImage',
        ['per-page']: 15
      }
    })
      .then(res => {
        setMaxPageCount(+res.headers.get('x-pagination-page-count'));
        setEventsArchive(res.data);
        eventsOld.current.scrollIntoView({block: 'start', behavior: 'smooth'});
      })
      .catch(err => {
        console.log(err)
      })

  };

  const handleClickFilterBtn = (year) => {
    let options = {
      query: {
        fields: 'name, date, slug',
        expand: 'thumbnailImage',
        ['per-page']: 15,
        year: year,
        page: 1
      }
    };

    if (year === 'Все') {
      delete options.query.year
    }

    fetchApi('/events/archive', {...options}, {isHeadersShow: true})
      .then(res => {
        setMaxPageCount(+res.headers.get('x-pagination-page-count'));
        setCurrentPage(1);
        setCurrentYear(year);
        setEventsArchive(res.data)
      })
      .catch(err => console.log(err));
  };

  const onReachedBottom = (isReachBottom) => {
    if (isReachBottom) {
      if (currentPage + 1 > maxPageCount) {
        return;
      }

      downloadNews();
    }
  };

  const downloadNews = () => {
    let options = {
      query: {
        fields: 'name, date, slug',
        expand: 'thumbnailImage',
        ['per-page']: 15,
        page: currentPage + 1
      }
    };

    if (currentYear !== 'Все') {
      options = {
        ...options,
        ...options.query = {...options.query, year: currentYear}
      }
    }

    fetchApi('/events/archive', {...options})
      .then((res) => {
        setEventsArchive([...eventsArchive, ...res.data]);
        setCurrentPage(currentPage + 1);
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      <Head>
        <title>{pageMeta.title || ''}</title>
        <meta name="keywords" content={pageMeta.keywords || ''}/>
        <meta name="description" content={pageMeta.description || ''}/>
      </Head>

      <div className="banner" style={{backgroundImage: `url(${bannerImage.url})`}}>
        <h1 className="banner__header">Афиша</h1>

        <div className="banner-text-wrapper">
          <div className="banner__date">{bannerSubHeader}</div>
          <h3 className="banner__title">{bannerHeader}</h3>
        </div>
        {bannerLink && <a href={bannerLink} className='open-event-link' target='_blank'/>}
      </div>

      <div className="events">
        {
          events.map((item, i) => {
            return (
              <Link href={`/event?slug=${item.slug}`} as={`/afisha/${item.slug}`} key={i}>
                <a className="events__item">
                  <div className="img"><img src={item.thumbnailImage.url} alt="alt"/></div>
                  <div className="date">{item.date}</div>
                  <h2 className="title">{item.name}</h2>
                </a>
              </Link>
            )
          })
        }

        <button className="events__archive-btn" onClick={handleClickEventsArchiveBtn}>
          <span className="title">Архив</span>
          <span className="description">Прошедшие события на SVOBODA2</span>
        </button>
      </div>

      {isArchiveVisible && <div className="events-old" ref={eventsOld}>
        <div className="filter">
          <button
            onClick={() => handleClickFilterBtn('Все')}
            className={'Все' === currentYear
              ? "filter__btn filter__btn_active"
              : "filter__btn"}>Все
          </button>
          {
            filterPoints.map((item, index) => {
              return <button
                onClick={() => handleClickFilterBtn(item)}
                className={item === currentYear
                  ? "filter__btn filter__btn_active"
                  : "filter__btn"}
                key={index}>
                {item}</button>
            })
          }
        </div>

        <VisibleByScroll
          onReachedBottom={onReachedBottom}>
          <div className="events-old-grid" ref={eventsOldGrid}>
            {
              eventsArchive.map((item) => {
                return (
                  <Link href={`/event?slug=${item.slug}`} as={`/afisha/${item.slug}`} key={item.slug}>
                    <a className="events__item">
                      <div className="img"><img src={item.thumbnailImage.url} alt={item.thumbnailImage.alt}/></div>
                      <div className="date">{item.date}</div>
                      <p className="title">{item.name}</p>
                    </a>
                  </Link>
                )
              })
            }
          </div>
        </VisibleByScroll>
      </div>}
    </>
  )
};

Bill.getInitialProps = async () => {
  const eventsPageData = await fetchApi('/events-page', {
    query: {
      fields: `banner_header, 
      banner_sub_header, 
      banner_link, 
      bannerImage, 
      events.date, 
      events.name, 
      events.slug, 
      events.thumbnailImage`,
      expand: `events, pageMeta, bannerImage, events.thumbnailImage`
    }
  });

  return eventsPageData.data;
};

export default withRedux(Bill, {classes: ['bill-page']});