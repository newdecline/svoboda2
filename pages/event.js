import React, {useState, useEffect} from 'react';
import {withRedux} from "../redux/withRedux";
import {fetchApi} from "../services/api/fetchApi";
import Head from "next/dist/next-server/lib/head";
import Swiper from 'react-id-swiper';
import Lightbox from 'react-image-lightbox';
import ArrowIcon from "../svg/arrow.svg";
import {disablePageScroll, enablePageScroll} from "scroll-lock";

const Event = props => {
    const {
        pageMeta,
        date,
        name,
        bannerImage: {url},
        description_link: descriptionLink,
        description_link_text: descriptionLinkText,
        description_contact: descriptionContact,
        description_place: descriptionPlace,
        description_price: descriptionPrice,
        description_text: descriptionText,
        gallery: {gallerySlides}
    } = props;

    const [photoIndex, setPhotoIndex] = useState(0);
    const [isOpenLightBox, setOpenLightBox] = useState(false);
    const [gallerySlider, updateGallerySlider] = useState(null);
    const [isBeginning, setBeginning] = useState(true);
    const [isEnd, setEnd] = useState(false);
    const [controls, setControls] = useState(true);

    const initialSettings = {
        speed: 400,
        slidesPerView: 1,
        watchSlidesProgress: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        },
    };

    const goNext = () => {
        if (gallerySlider !== null) {
            gallerySlider.slideNext();
        }
    };

    const goPrev = () => {
        if (gallerySlider !== null) {
            gallerySlider.slidePrev();
        }
    };

    const handleClickSliderLeaf = index => {
        setPhotoIndex(index);
        setOpenLightBox(true);
    };

    useEffect(() => {
        if (gallerySlider !== null) {
            if (gallerySlider.slides.length <= 1) {
                setControls(false);
                document.body.classList.add('no-controls-light-box');
            }

            gallerySlider.on('transitionStart', function () {
                setBeginning(gallerySlider.isBeginning);
                setEnd(gallerySlider.isEnd);
            });
        }
    }, [gallerySlider]);

    useEffect(() => {
        if (isOpenLightBox) {
            disablePageScroll()
        } else {
            enablePageScroll()
        }
    }, [isOpenLightBox]);

    return (
        <>
            <Head>
                <title>{pageMeta.title || ''}</title>
                <meta name="keywords" content={pageMeta.keywords || ''} />
                <meta name="description" content={pageMeta.description || ''} />
            </Head>

            <div className="banner" style={{backgroundImage: `url(${url})`}}>
                <div className="banner-text-wrapper">
                    <div className="banner__date">{date}</div>
                    <h3 className="banner__title">{name}</h3>
                </div>
            </div>

            <main className="master-class">
                <div className="master-class__text-wrapper">
                    <h3 className="master-class__name">{name}</h3>

                    <div className="master-class__date">{date}</div>

                    {descriptionLinkText && <div className="master-class__owner-wrapper">
                        <a href={descriptionLink} target="_blank" className="master-class__owner">{descriptionLinkText}</a>
                    </div>}

                    <div className="master-class__where">{descriptionPlace}</div>

                    {descriptionPrice && <div className="master-class__cost"><b>Стоимость участия:</b> {descriptionPrice}</div>}

                    {descriptionContact &&  <div className="master-class__tel"><b>Записаться:</b> {descriptionContact}</div>}

                    <p className="master-class__description">{descriptionText}</p>
                </div>

                {!gallerySlides.length
                    ? null
                    : <div
                        className={controls
                            ? "master-class__gallery"
                            : "master-class__gallery no-controls"}>
                        <button disabled={isBeginning} className="swiper-button-prev" onClick={goPrev}><ArrowIcon/></button>
                        <button disabled={isEnd} className="swiper-button-next" onClick={goNext}><ArrowIcon/></button>

                        <Swiper
                            {...initialSettings}
                            getSwiper={updateGallerySlider}>
                            {
                                gallerySlides.map((item, index) => {
                                    return (
                                        <div key={index} className="slider-leaf">
                                            <div
                                                className="wrapper-img"
                                                onClick={ () => handleClickSliderLeaf(index) }>
                                                <img
                                                    className="img"
                                                    src={item.thumbnailImage.url}
                                                    alt={item.thumbnailImage.alt}
                                                />
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </Swiper>
                    </div>}
            </main>

            {isOpenLightBox && <Lightbox
                mainSrc={gallerySlides[photoIndex].originalImage.url}
                nextSrc={gallerySlides[(photoIndex + 1) % gallerySlides.length].originalImage.url}
                prevSrc={gallerySlides[(photoIndex + gallerySlides.length - 1) % gallerySlides.length].originalImage.url}
                imageTitle={gallerySlides[photoIndex].text}
                onCloseRequest={() => setOpenLightBox(false)}
                onMovePrevRequest={() => setPhotoIndex((photoIndex + gallerySlides.length - 1) % gallerySlides.length)}
                onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % gallerySlides.length)}
                nextLabel={'Следущая картинка'}
                prevLabel={'Предыдущая картинка'}
            />}
        </>
    )
};

Event.getInitialProps = async (res) => {
    const {slug} = res.query;

    const eventPageData = await fetchApi(`/events/published/${slug}`, {query: {
            expand: `pageMeta, bannerImage, gallery.gallerySlides`
        }
    });

    if (eventPageData.status === 404) {
        res.statusCode = 404;

        return {
            error: {
                statusCode: 404
            }
        };
    }

    return eventPageData.data;
};

export default withRedux(Event, {classes: ['page-event']});