import React, {useState, useEffect} from "react";
import {withRedux} from "../redux/withRedux";
import {fetchApi} from "../services/api/fetchApi";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import Head from "next/head";
import ArrowIcon from "../svg/arrow.svg";
import Swiper from "react-id-swiper";
import Lightbox from "react-image-lightbox";

export const RoomPage = props => {
  const {
    pageMeta,
    name,
    bannerImage: {url},
    location,
    square,
    cost,
    phone,
    description,
    gallery: {gallerySlides}
  } = props;

  const [photoIndex, setPhotoIndex] = useState(0);
  const [isOpenLightBox, setOpenLightBox] = useState(false);
  const [gallerySlider, updateGallerySlider] = useState(null);
  const [isBeginning, setBeginning] = useState(true);
  const [isEnd, setEnd] = useState(false);
  const [controls, setControls] = useState(true);

  const initialSettings = {
    speed: 400,
    slidesPerView: 1,
    watchSlidesProgress: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
  };

  const goNext = () => {
    if (gallerySlider !== null) {
      gallerySlider.slideNext();
    }
  };

  const goPrev = () => {
    if (gallerySlider !== null) {
      gallerySlider.slidePrev();
    }
  };

  const handleClickSliderLeaf = index => {
    setPhotoIndex(index);
    setOpenLightBox(true);
  };

  useEffect(() => {
    if (gallerySlider !== null) {
      if (gallerySlider.slides.length <= 1) {
        setControls(false);
        document.body.classList.add('no-controls-light-box');
      }

      gallerySlider.on('transitionStart', function () {
        setBeginning(gallerySlider.isBeginning);
        setEnd(gallerySlider.isEnd);
      });
    }
  }, [gallerySlider]);

  useEffect(() => {
    if (isOpenLightBox) {
      disablePageScroll()
    } else {
      enablePageScroll()
    }
  }, [isOpenLightBox]);

  const costValue = cost.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

  return <>
    <Head>
      <title>{pageMeta.title || ''}</title>
      <meta name="keywords" content={pageMeta.keywords || ''} />
      <meta name="description" content={pageMeta.description || ''} />
    </Head>

    <div className="banner" style={{backgroundImage: `url(${url})`}}/>

    <main className="content">
      <div className="content__text-wrapper">
        <h3 className="name">{name}</h3>

        <ul className="feature-list">
          {location && <li className="feature-list__item">
            <span className="item-title">Расположение: </span>
            <span className="item-description">{location}</span>
          </li>}
          {square && <li className="feature-list__item">
            <span className="item-title">Площадь: </span>
            <span className="item-description">{square} кв.м.</span>
          </li>}
          {costValue && <li className="feature-list__item">
            <span className="item-title">Стоимость: </span>
            <span className="item-description">{costValue} руб./месяц</span>
          </li>}
          {phone && <li className="feature-list__item">
            <span className="item-title">Отдел аренды: </span>
            <a href={`tel:${phone}`} className="item-description">{phone}</a>
          </li>}
        </ul>

        {description && <p className="description">{description}</p>}
      </div>

      {!gallerySlides.length
        ? null
        : <div
          className={controls
            ? "gallery"
            : "gallery no-controls"}>
          <button disabled={isBeginning} className="swiper-button-prev" onClick={goPrev}><ArrowIcon/></button>
          <button disabled={isEnd} className="swiper-button-next" onClick={goNext}><ArrowIcon/></button>

          <Swiper
            {...initialSettings}
            getSwiper={updateGallerySlider}>
            {
              gallerySlides.map((item, index) => {
                return (
                  <div key={index} className="slider-leaf">
                    <div
                      className="wrapper-img"
                      onClick={ () => handleClickSliderLeaf(index) }>
                      <img
                        className="img"
                        src={item.thumbnailImage.url}
                        alt={item.thumbnailImage.alt}
                      />
                    </div>
                  </div>
                )
              })
            }
          </Swiper>
        </div>}
    </main>

    {isOpenLightBox && <Lightbox
      mainSrc={gallerySlides[photoIndex].originalImage.url}
      nextSrc={gallerySlides[(photoIndex + 1) % gallerySlides.length].originalImage.url}
      prevSrc={gallerySlides[(photoIndex + gallerySlides.length - 1) % gallerySlides.length].originalImage.url}
      imageTitle={gallerySlides[photoIndex].text}
      onCloseRequest={() => setOpenLightBox(false)}
      onMovePrevRequest={() => setPhotoIndex((photoIndex + gallerySlides.length - 1) % gallerySlides.length)}
      onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % gallerySlides.length)}
      nextLabel={'Следущая картинка'}
      prevLabel={'Предыдущая картинка'}
    />}
  </>
};

RoomPage.getInitialProps = async (res) => {
  const {slug} = res.query;

  const roomPageData = await fetchApi(`/rooms/published/${slug}`, {query: {
      expand: `pageMeta, bannerImage, gallery.gallerySlides`
    }
  });

  if (roomPageData.status === 404) {
    res.statusCode = 404;

    return {
      error: {
        statusCode: 404
      }
    };
  }

  return roomPageData.data;
};

export default withRedux(RoomPage, {classes: ['room-page']});