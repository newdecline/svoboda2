import React, {useState, useEffect} from "react";
import {withRedux} from "../redux/withRedux";
import classnames from "classnames";
import Link from "next/link";
import {Portal} from 'react-portal';
import {enablePageScroll, disablePageScroll} from 'scroll-lock';
import {CompaniesFilter} from "../components/CompaniesFilter";
import PhoneIcon from "../svg/phone-with-circle.svg";
import LocationIcon from "../svg/location.svg";
import CrossIcon from "../svg/cross.svg";
import {fetchApi} from "../services/api/fetchApi";
import Head from "next/head";

const CompaniesPage = props => {
  const {
    pageData: {
      bannerImage,
      companiesByTags,
      pageMeta
    }
  } = props;

  const [categoryActive, setCategoryActive] = useState('');

  const [displayType, setDisplayType] = useState('list');

  const [locationImage, setLocationImage] = useState({});
  useEffect(() => {
    if (locationImage.url) {
      disablePageScroll();
    } else {
      enablePageScroll();
    }
  }, [locationImage.url]);

  const onChangeFilter = value => {
    setCategoryActive(value)
  };

  const onChangeDisplayType = value => {
    setDisplayType(value);
  };

  const handleClickLocationBtn = (locationImage) => {
    locationImage && setLocationImage(locationImage);
  };

  const handleClickDrivingDirectionsPopup = e => {
    if (e.target.dataset.closePopup) {
      setLocationImage({});
    }
  };

  const categoriesList = companiesByTags.map((item) => {
    return {label: item.tagName, value: item.tagId}
  });

  return <>
    <Head>
      <title>{pageMeta.title || ''}</title>
      <meta name="keywords" content={pageMeta.keywords || ''}/>
      <meta name="description" content={pageMeta.description || ''}/>
    </Head>

    <div>
      <div className="banner" style={{backgroundImage: `url(${bannerImage.url})`}}>
        <h1 className="banner__header">Жители</h1>
      </div>

      <div className="content">
        <CompaniesFilter
          onChangeFilter={onChangeFilter}
          displayType={displayType}
          categoryActive={categoryActive}
          categoriesList={categoriesList}
          onChangeDisplayType={onChangeDisplayType}
        />
        <div className={classnames('companies-container', {'tile': displayType === 'tile'})}>
          {companiesByTags.map((props) => {
            return (
              <Category
                key={props.tagId}
                displayType={displayType}
                handleClickLocationBtn={handleClickLocationBtn}
                {...props}
              />)
          })}
        </div>
      </div>
    </div>


    {locationImage.url && <Portal>
      <div data-close-popup={true} className="driving-directions" onClick={handleClickDrivingDirectionsPopup}>
        <button onClick={() => setLocationImage({})} className="close"><CrossIcon/></button>
        <img
          src={locationImage.url}
          alt={locationImage.alt}/>
      </div>
    </Portal>}
  </>
};

CompaniesPage.getInitialProps = async () => {
  const rentPageData = await fetchApi('/companies-page', {
    query: {
      expand: `companiesByTags, bannerImage, pageMeta`
    }
  });

  return {
    pageData: rentPageData.data
  };
};

export default withRedux(CompaniesPage, {classes: ['companies-page']});

const Category = props => {
  const {
    tagName,
    companies,
    handleClickLocationBtn
  } = props;

  return <div className='category-wrapper' data-category-id={props.tagId}>
    <div className="category-name">{tagName}</div>
    <div className="company-blocks">
      {companies.map((props, i) => {
        return (
          <Company
            key={i}
            handleClickLocationBtn={handleClickLocationBtn}
            {...props}/>
        )})}
    </div>
  </div>
};

const Company = props => {
  const {
    handleClickLocationBtn,
    location,
    locationImage,
    avatarImage,
    list_name,
    phone,
    slug,
  } = props;

  return <>
    <div className='company-block'>
      <Link href={`/company?slug=${slug}`} as={`/zhiteli/${slug}`}>
        <a className="location-image"><img src={avatarImage.url} alt={avatarImage.alt}/></a>
      </Link>

      <div className={classnames('company-block-row', {'no-phone': !phone})}>
        <div className="wrapper-link-page">
          <Link href={`/company?slug=${slug}`} as={`/zhiteli/${slug}`}>
            <a className="company-block__item name">{list_name}</a>
          </Link>
          <span className="company-block__item-text_view-mobile">{location}</span>
        </div>

        {phone && <a className='company-block__item phone' href={`tel:${phone}`}>
          <span className="company-block__item-text" title={phone}>{phone}</span> <PhoneIcon/>
        </a>}
        {location && <div className={classnames("company-block__item address", {'no-icon-map': !locationImage.url})} onClick={() => handleClickLocationBtn(locationImage)}>
          <span className="company-block__item-text" title={location}>{location}</span>{locationImage.url && <LocationIcon/>}
        </div>}
      </div>

    </div>
  </>
};