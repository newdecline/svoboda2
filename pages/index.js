import React, {useState, useEffect, useRef} from "react";
import Head from 'next/head';
import {AboutSection} from "../components/Sections/AboutSection";
import {ContactsSection} from "../components/Sections/ContactsSection";
import {withRedux} from "../redux/withRedux";
import {fetchApi} from '../services/api/fetchApi';
import {EventsSection} from "../components/Sections/EventsSection";
import {HoldEventSection} from "../components/Sections/HoldEventSection";
import {LocationSection} from "../components/Sections/LocationSection";
import {HeaderSlider} from "../components/HeaderSlider";
import {IconsSocial} from "../components/IconsSocial";
import Scrollchor from 'react-scrollchor';
import FontFaceObserver from 'fontfaceobserver';
import Swiper from "react-id-swiper";
import {RentSection} from "../components/Sections/RentSection";
import {CompaniesSection} from "../components/Sections/CompaniesSection";

const Index = props => {
  const colLeftTitle = useRef(null);

  const [isLoadFonts, setLoadFonts] = useState(false);
  const [glitchTextSlider, updateGlitchTextSlider] = useState(null);

  const {
    offer_1: offer1,
    offer_2: offer2,
    offer_3: offer3,
    about_section_header: aboutSectionHeader,
    about_text: aboutText,
    contacts_address: contactsAddress,
    contacts_coordinates: contactsCoordinates,
    contacts_email: contactsEmail,
    contacts_phone_1: contactsPhone1,
    contacts_phone_2: contactsPhone2,
    contacts_section_header: contactsSectionHeader,
    feedbackForm,
    areaForm,
    mainBanner,
    pageMeta,
    show_about_section: showAboutSection,
    show_events_section: showEventsSection,
    show_own_event_section: showOwnEventSection,
    aboutGallery,
    events_section_header: eventsSectionHeader,
    events_text: eventsDescriptions,
    events,
    areas,
    ownEventImage,
    own_event_section_header: ownEventSectionHeader,
    own_event_text: ownEventDescriptions,
    show_rent_section,
    rent_section_header: rentSectionHeader,
    rent_text: rentDescriptions,
    rooms,
    show_companies_section,
    companies_section_header,
    companies_text,
    companiesImage,
    layoutData: {
      instagram_url: instagramUrl,
      vk_url: vkUrl,
      email
    }
  } = props;

  const initSettingsGlitchTextSlider = {
    loop: true,
    loopedSlides: 1,
    speed: 1000,
    slidesPerView: 1
  };

  const onChangeBannerSlider = direction => {
    if (direction === 'next') {
      glitchTextSlider.slideNext(1000);
    } else {
      glitchTextSlider.slidePrev(1000);
    }
  };

  useEffect(() => {
    const font = new FontFaceObserver('Bebas-Tam-Bold', {
      weight: 400
    });

    font.load().then(function () {
      setLoadFonts(true);
    }, function (err) {
      console.log(err);
    });

    if (window.matchMedia("(max-width: 1023px)").matches) {
      if (colLeftTitle.current) {
        const realVh = document.documentElement.clientHeight;
        const $anchorLink = document.querySelector('.anchor-link');

        if ($anchorLink) {
          $anchorLink.style.top = `${realVh - 60}px`;
        }
        colLeftTitle.current.style.top = `${realVh - 230}px`;
      }
    }
  }, []);

  useEffect(() => {
    if (glitchTextSlider !== null) {
      glitchTextSlider.update()
    }
  }, [glitchTextSlider]);

  return (
    <>
      <Head>
        <title>{pageMeta.title || ''}</title>
        <meta name="keywords" content={pageMeta.keywords || ''}/>
        <meta name="description" content={pageMeta.description || ''}/>
      </Head>

      <h2
        className={isLoadFonts
          ? "banner-main-page__title animate-title"
          : "banner-main-page__title"}>Свобода</h2>

      <div id='glavnaya'>
        <div className="banner-main-page">
          <HeaderSlider
            bannerSlides={mainBanner.bannerSlides}
            isLoadFonts={isLoadFonts}
            onChangeBannerSlider={onChangeBannerSlider}/>

          <Scrollchor
            animate={{offset: 0, duration: 200}}
            to="#offers"
            className="anchor-link">
            <span></span>
          </Scrollchor>


          <div className="col-left">
            {
              mainBanner.bannerSlides.length !== 0 &&
              <div
                className={isLoadFonts
                  ? "slider-text slider-text-glitch once-start-animate"
                  : "slider-text slider-text-glitch"}>
                <Swiper
                  {...initSettingsGlitchTextSlider}
                  getSwiper={updateGlitchTextSlider}>
                  {mainBanner.bannerSlides.map((slide, index) => (
                    <div
                      key={index}
                      className="slider-text__leaf">
                      <h2 className="title glitch" data-text={slide.title}>{slide.title}</h2>
                    </div>
                  ))}
                </Swiper>
              </div>
            }

            <IconsSocial socialLinks={[instagramUrl, vkUrl]}/>
            <h2
              ref={colLeftTitle}
              className="col-left__title">{`Центр\nкреативных\nиндустрий`}</h2>
          </div>

          <div id='offers' className="offers">
            {[offer1, offer2, offer3].map((offer, index) => (
              <div key={index} className="offers__item">{offer}</div>
            ))}
          </div>

          <a href={`mailto:${email}`} className="mail">{email}</a>
        </div>
      </div>

      {
        showAboutSection &&
        <AboutSection
          aboutSectionHeader={aboutSectionHeader}
          aboutText={aboutText}
          aboutGallery={aboutGallery}
        />
      }

      {
        showEventsSection &&
        <EventsSection
          eventsSectionHeader={eventsSectionHeader}
          eventsDescriptions={eventsDescriptions}
          events={events}/>
      }

      {
        showOwnEventSection &&
        <HoldEventSection
          ownEventImage={ownEventImage}
          ownEventSectionHeader={ownEventSectionHeader}
          ownEventDescriptions={ownEventDescriptions}/>
      }

      {
        areas.length !== 0 &&
        <div>
          {
            areas.map((area, idx) => {
              return (
                <LocationSection key={idx} areaForm={areaForm} {...area}/>
              )
            })
          }
        </div>
      }

      {show_rent_section && (
        <RentSection
          rooms={rooms}
          rentSectionHeader={rentSectionHeader}
          rentDescriptions={rentDescriptions}/>)}

      {show_companies_section && (
        <CompaniesSection
          bannerImage={companiesImage}
          title={companies_section_header}
          description={companies_text}/>)}

      <ContactsSection
        contactsSectionHeader={contactsSectionHeader}
        contactsAddress={contactsAddress}
        contactsPhone1={contactsPhone1}
        contactsPhone2={contactsPhone2}
        contactsEmail={contactsEmail}
        contactsCoordinates={contactsCoordinates}
        feedbackForm={feedbackForm}
      />
    </>
  );
};

Index.getInitialProps = async () => {
  const homePageData = await fetchApi('/home-page', {
    query: {
      expand: `pageMeta, 
            mainBanner, 
            areas, 
            aboutGallery, 
            events,
            events.thumbnailImage, 
            rooms,
            rooms.thumbnailImage,
            ownEventImage, 
            feedbackForm, 
            areaForm,
            show_companies_section,
            companies_section_header,
            companies_text,
            companiesImage`,
    }
  });

  return homePageData.data;
};

export default withRedux(Index, {classes: ['page-index']});