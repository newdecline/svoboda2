import React from 'react'
import  '../sass/index.scss';
import {withRedux} from "../redux/withRedux";

function Error({ statusCode }) {
    return (
        <div className='error-status'>
            {statusCode}
        </div>
    )
}

Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
    return { statusCode }
};

export default withRedux(Error, {classes: ['error-page']});