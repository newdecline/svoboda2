import React, {useState, useEffect, useRef} from "react";
import {withRedux} from "../redux/withRedux";
import {fetchApi} from "../services/api/fetchApi";
import Head from "next/head";
import Link from "next/link";

const RoomsPage = props => {
  const {
    pageData: {pageMeta, bannerImage},
    roomsData,
    maxPageCount
  } = props;

  let $footer;

  const roomsContainer = useRef(null);

  if (process.browser) {
    $footer = document.querySelector('.page-footer');
  }

  const [currentPage, setCurrentPage] = useState(1);
  const [rooms, setRooms] = useState(roomsData);
  const [loadingData, setLoadingData] = useState(false);

  const [reachBottom, setReachBottom] = useState(false);
  useEffect(() => {
    if (reachBottom) {
      if (currentPage === maxPageCount && currentPage + 1 > maxPageCount) {return}
      if (currentPage !== 1 && (currentPage + 1 > maxPageCount)) {return}
      !loadingData && fetchRooms();
    }
  }, [reachBottom]);

  const fetchRooms = () => {
    let options = {
      query: {
        fields: 'thumbnailImage, cost, name, slug, square',
        expand: 'thumbnailImage, cost, name, slug, square',
        ['per-page']: 12,
        page: currentPage + 1
      }
    };

    fetchApi('/rooms/published', {...options})
      .then((res) => {
        setLoadingData(true);
        setCurrentPage(currentPage + 1);
        setRooms([...rooms, ...res.data]);
      })
      .then(() => {
        setLoadingData(false);
        setReachBottom(false);
      })
      .catch(err => console.log(err));
  };

  const onScroll = () => {
    if (roomsContainer !== null) {
      if (roomsContainer.current.offsetHeight + $footer.offsetHeight - 300 < window.innerHeight + document.documentElement.scrollTop) {
        setReachBottom(true);
      }
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', onScroll);
    return () => {window.removeEventListener('scroll', onScroll);}
  }, []);

  return <>
    <Head>
      <title>{pageMeta.title || ''}</title>
      <meta name="keywords" content={pageMeta.keywords || ''}/>
      <meta name="description" content={pageMeta.description || ''}/>
    </Head>

    <div className="banner" style={{backgroundImage: `url(${bannerImage.url})`}}>
      <h1 className="banner__header">Аренда</h1>
    </div>


    <div className="rent-grid-container" ref={roomsContainer}>
      {rooms.map((item, i) => {
        const cost = item.cost.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        return (
          <Link href={`/room?slug=${item.slug}`} as={`/arenda/${item.slug}`} key={item.slug}>
            <a className="rent-grid-container__item">
              {item.thumbnailImage && <div className="img"><img src={item.thumbnailImage.url} alt="alt"/></div>}
              <div className="cost">
                {cost} руб.
                <span className="cost__divider"/>
                {item.square} кв.м.
              </div>
              <h2 className="title">{item.name}</h2>
            </a>
          </Link>
        )
      })}
    </div>
  </>
};

RoomsPage.getInitialProps = async () => {
  const rentPageData = await fetchApi('/rent-page', {
    query: {
      expand: `pageMeta, bannerImage`
    }
  });

  const roomsData = await fetchApi('/rooms/published', {
    query: {
      fields: `thumbnailImage, cost, name, slug, square`,
      expand: `thumbnailImage, cost, name, slug, square`,
      ['per-page']: 12,
    }
  });

  return {
    pageData: rentPageData.data,
    roomsData: roomsData.data,
    maxPageCount: +roomsData.headers.get('x-pagination-page-count')
  };
};

export default withRedux(RoomsPage, {classes: ['rent-page']});