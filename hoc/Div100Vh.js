import React, {useEffect} from "react";

export const Div100Vh = props => {
    const {children} = props;

    let vh, vw, isTouch;

    if (process.browser) {
        isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
    }

    const calculateScrollBarWidth = () => {
        return window.innerWidth - document.body.clientWidth;
    };

    const start = () => {
        vh = document.documentElement.clientHeight * 0.01;
        vw = document.documentElement.clientWidth * 0.01;

        document.documentElement.style.setProperty("--vh", `${vh}px`);
        document.documentElement.style.setProperty("--vw", `${vw}px`);
        !isTouch && document.documentElement.style.setProperty("--scBarWidth", `${calculateScrollBarWidth()}px`);
    };

    useEffect(() => {
        start();
        window.addEventListener("resize", start);

        return () => {
            window.removeEventListener("resize", start);
        }
    }, []);

    return (
        <>
            {children}
        </>
    )
};

/* Usage example */
// top: calc(var(--vh, 1vh) * 90);
// width: calc(var(--vw, 1vw) * 50 + 17px);